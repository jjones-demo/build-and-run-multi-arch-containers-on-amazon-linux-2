On amd64, should be able to build linux/386, linux/arm64, linux/riscv64, linux/ppc64le, linux/s390x, linux/arm/v7, linux/arm/v6

The cost of hosting runners on some alternative archs could be substantial - especially if compared to spot.  Doing builds on Amazon Linux 2 allows runners to consume cheaper compute and alternative arch hardware deployment to be limited to production environments.

If this docker configuration is placed on a machine with the shell runner, the special `docker buildx` commands could be accommodated.

This approach works on other platform as well - the code below has simply been validated on Amazon Linux 2.

Setting up multiplatform docker builds on Amazon Linux from heading starting with "Creating a multi-arch image builder": https://aws.amazon.com/blogs/compute/how-to-quickly-setup-an-experimental-environment-to-run-containers-on-x86-and-aws-graviton2-based-amazon-ec2-instances-effort-to-port-a-container-based-application-from-x86-to-graviton2/

Some bits for build from: https://www.docker.com/blog/multi-platform-docker-builds/

```
sudo su
yum install -y docker git
systemctl start docker

export DOCKER_BUILDKIT=1
docker build --platform=local -o . git://github.com/docker/buildx
mkdir -p ~/.docker/cli-plugins
mv buildx ~/.docker/cli-plugins/docker-buildx
chmod a+x ~/.docker/cli-plugins/docker-buildx

docker run --privileged --rm tonistiigi/binfmt --install all

docker buildx ls

docker buildx create --name mybuild --use
docker buildx inspect --bootstrap

#docker login #if necessary to get to dockerhub

cat > Dockerfile <<'EOF'
FROM ppc64le/bash
CMD echo "Hello World from platform: '$(uname -m)'"
EOF

docker buildx build --platform ppc64le -t local-build . --load

docker run --platform ppc64le --rm local-build

```
